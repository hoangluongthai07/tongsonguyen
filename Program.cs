﻿using System;

class Program
{
    static void Main()
    {   
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        Console.WriteLine("Nhập số nguyên N: ");
        int N = int.Parse(Console.ReadLine());

        int sum = SumEvenNumbers(N);
        Console.WriteLine($"Tổng các số chẵn từ 1 đến {N} là {sum}.");
    }

    static int SumEvenNumbers(int N)
    {
        int sum = 0;

        for (int i = 2; i <= N; i += 2)
        {
            sum += i;
        }

        return sum;
    }
}
